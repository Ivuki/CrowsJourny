﻿using UnityEditor;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool correctChoice = false;
    public GameObject window;
    public Sprite changeSprite;
    
    private int _wasHere = 0;
    private SpriteRenderer _spriteRenderer;
    private Sprite _startingSprite;
    private bool _changeFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _startingSprite = _spriteRenderer.sprite;
        
    }

    void OnMouseOver()
    {
        if (!Input.GetMouseButtonDown(0)) return;

        _spriteRenderer.sprite = _changeFlag ? _startingSprite : changeSprite;
        _changeFlag = !_changeFlag;

        if (!correctChoice)
        {
            Debug.Log("WRONG LEVEEEEEEER");
        }
        else
        {
            window.GetComponent<WindowScript>().isOpen = !window.GetComponent<WindowScript>().isOpen;
            Debug.Log("CORRECT THE NUMBER IS 4");
        }
        
    }
}
