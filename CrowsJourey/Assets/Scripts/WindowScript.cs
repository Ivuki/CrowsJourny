﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;

public class WindowScript : MonoBehaviour
{

    public Sprite open;
    public bool isOpen = false;

    private SpriteRenderer _spriteRenderer;

    private Sprite _closed;
    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _closed = _spriteRenderer.sprite;

    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            _spriteRenderer.sprite = open;
        }
        else
        {
            _spriteRenderer.sprite = _closed;
        }
        
    }
}
